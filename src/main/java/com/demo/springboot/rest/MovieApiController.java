package com.demo.springboot.rest;

import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.service.Movie;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@RestController
public class MovieApiController {
    private static final Logger LOG = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    Movie movies;

    @GetMapping("/movies")
    public ResponseEntity<List> getMovies() {
        LOG.info("--- get all movies: {}", movies);
        return ResponseEntity.ok().body(movies.moviesList);    // = new ResponseEntity<>(movies, HttpStatus.OK);
    }

    @PutMapping("/movies")
    public ResponseEntity<Void> updateMovie(@RequestParam("id") Integer id, @RequestBody MovieDto movieDto) {
        LOG.info("--- id: {}", id);
        LOG.info("--- title: {}", movieDto.getTitle());

        movies.updateMovie(id,movieDto);

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/movies")
    public ResponseEntity<Void> deleteMovie(@RequestParam("id") Integer id) {
        movies.deleteMovie(id);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/movies")
    public ResponseEntity<Void> createMovie(@RequestBody MovieDto movieDto) throws URISyntaxException {
        LOG.info("--- id: {}", movieDto.getMovieId());
        LOG.info("--- title: {}", movieDto.getTitle());
        movies.addMovie(movieDto);

        return ResponseEntity.created(new URI("/movies/" + movieDto.getMovieId())).build();
    }
}
