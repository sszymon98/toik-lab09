package com.demo.springboot.service;

import com.demo.springboot.dto.MovieDto;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public interface Movie {
    public List<MovieDto> moviesList = new ArrayList<>();
    void addMovie(MovieDto movieDto);
    void updateMovie(int movieId, MovieDto movieDto);
    void deleteMovie(int movieId);
}
