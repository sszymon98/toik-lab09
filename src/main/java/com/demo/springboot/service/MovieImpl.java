package com.demo.springboot.service;

import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieListDto;
import org.springframework.context.annotation.Configuration;


@Configuration
public class MovieImpl implements Movie{
    public MovieImpl(){
        moviesList.add(new MovieDto(1,
                "Piraci z Krzemowej Doliny",
                1999,
                "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
        );
        new MovieListDto(moviesList);
    }

    public void addMovie(MovieDto movieDto){
        movieDto.setMovieId(moviesList.size()+1);
        moviesList.add(movieDto);
        new MovieListDto(moviesList);
    }

    public void updateMovie(int movieId, MovieDto movieDto){
        try {
            moviesList.get(movieId - 1).setTitle(movieDto.getTitle());
            moviesList.get(movieId - 1).setYear(movieDto.getYear());
            moviesList.get(movieId - 1).setImage(movieDto.getImage());
        }catch (Exception e){
            System.out.println("Element not found!");
        }
    }

    public void deleteMovie(int movieId){
        moviesList.remove(movieId-1);
    }

}
